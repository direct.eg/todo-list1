import React, { useState } from 'react';

const Form = (props) => {

  const [name, setName] = useState('');

  function handleChange(e){
    setName(e.target.value  );
  }

  function handleSubmit(e){
    e.preventDefault();
    props.addTask(name);
    setName('');
  }

  return (
    <form onSubmit={ handleSubmit  } >
      <input type="text" name="task-name" className="input input_lg" value={name} onChange={handleChange} />
      <button type="submit" className='btn btn_primary btn_lg' >Add</button>
    </form>
  );
};

export default Form;