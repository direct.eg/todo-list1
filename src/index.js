import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';

const DATA = [
  {id: "todo-0", name: "Eat", status:true},
  {id: "todo-1", name: "Sleep", status:false},
  {id: "todo-2", name: "Repeat", status:false}  
];

ReactDOM.render(<App tasks={DATA} />, document.getElementById('root'));