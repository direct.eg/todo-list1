import { useState } from "react";
import Filter from "./components/Filter";
import Form from "./components/Form";
import Todo from "./components/Todo";

function App(props) {

  const [tasks, setTasks] = useState(props.tasks);

  const taskList = tasks.map((elt) => <Todo
    name={elt.name}
    status={elt.status}
    id={elt.id}
    key={elt.id}
    deleteTask={deleteTask}
    toggleTaskCompleted={toggleTaskCompleted}
  />
  );

  function toggleTaskCompleted(id) {
    
    const updateTasks = tasks.map((task) => {
      if (id === task.id) {
        return { ...task, completed: !task.status }
      }
      return task;
    });
    setTasks(updateTasks);
  }

  function deleteTask(id) {
    const remainingTasks = tasks.filter((task) => id !== task.id);
    setTasks(remainingTasks);
  }

  function addTask(taskName) {
    const newTask = { id: `todo-${tasks.length}`, name: taskName, status: false };
    setTasks([...tasks, newTask]);
  }

  const tasksNb = tasks.length !== 1 ? 'tasks' : 'task';
  const taskLabel = `${tasks.length} ${tasksNb} remaining`;

  return (
    <div className="todoapp stack-large">
      <h1>What needs to be done?</h1>

      <Form addTask={addTask} />

      <div className="filters btn-group" >
        <Filter name="All" />
        <Filter name="Active" />
        <Filter name="Completed" />
      </div>

      <h2> {taskLabel} </h2>

      <ul className="todo-list stack-large stack-exception" >

        {taskList}

      </ul>

    </div>
  );
}

export default App;
